from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy_utils import database_exists, create_database

DB_USER = 'postgres'
DB_PASSWORD = 'postgres'
DB_NAME = 'test23'
DB_ECHO = True

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = f'postgresql://{DB_USER}:{DB_PASSWORD}@localhost/{DB_NAME}'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class Student(db.Model):
   id = db.Column('student_id', db.Integer, primary_key=True)
   name = db.Column(db.String(100))
   city = db.Column(db.String(50))
   address = db.Column(db.String(200))
   pin = db.Column(db.String(10))

   def __init__(self, name, city, address, pin):
       self.name = name
       self.city = city
       self.address = address
       self.pin = pin


if not database_exists(db.engine.url):
  create_database(db.engine.url)
db.init_app(app)
db.create_all()


@app.route('/')
def students():
    students = Student.query.all()
    return render_template('students.html', students=students)


if __name__ == '__main__':
    app.run(debug=True)
